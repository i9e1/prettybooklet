# pretty booklet

Bash script to convert a pdf to 
a booklet with subbooklets in one pdf
for easy printing and binding / stitching.

## requirements

* ```pdftk```
* ```gs``` (ghostscript)
* ```pdfbook``` (> pdfjam > texlive)

## license

distributed under EUPL v1.2

https://www.eupl.eu/1.2/de/
https://www.eupl.eu/1.2/en/
