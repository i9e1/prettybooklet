#!/bin/bash
#
# script to create a booklet with
# pages packed for printing
# and nice binding / stitching of subbooklets
#
# Hans Stenglein, 26.07.22
#
# distributed under EUPL v1.2

# TODO --help
# TODO extra arg for number of sheets per subbooklet
# TODO sanity check of args, check if pdf exists and num is /4
# exit 1 if not!
PDF=$1
PAGECOUNT=$( pdftk "$PDF" dump_data |
  grep NumberOfPages | awk '{ print $2 }' )

# TODO use temp dir
#TMP = $PWD/tmp/
#mkdir -p $TMP
#pdftk $PDF burst output $TMP/source-%d.pdf

# TODO cleanup code
# TODO suppress outputs
# TODO progress bar

# stitch N = 4 'pages' * 4 papers
NSTITCH=16
FULLSTITCH=$(( PAGECOUNT / NSTITCH ))
REMAINDER=$(( PAGECOUNT % NSTITCH ))
if [ $REMAINDER -gt 0 ]; then FULLSTITCH=$(( FULLSTITCH + 1)); fi
echo ">> booklets are created with $(( NSTITCH / 4 )) sheets = $NSTITCH pages"
#echo $FULLSTITCH

# remainder, fill with blank pages
# -g5950x8420 = A4
# -c count (NSTITCH - REMAINDER) blank pages
gs -sDEVICE=pdfwrite -o blank.pdf \
  -dDEVICEWIDTHPOINTS=612 -dDEVICEHEIGHTPOINTS=792 -g5950x8420 \
  -c $(( NSTITCH - REMAINDER )) "{showpage}" repeat
echo ">> appending $(( NSTITCH - REMAINDER )) blank pages"

TOBIND="${PDF/%.pdf/}"-tobind.pdf
pdftk "$PDF" blank.pdf cat output "$TOBIND"

# pages start with 1
BLOCK=1
STITCHED=""
# fully printed booklets
while [ $BLOCK -le $(( FULLSTITCH * NSTITCH )) ]
do
  ENDBLOCK=$(( BLOCK + NSTITCH - 1 ))
  TMP=binding_"$BLOCK"_"$ENDBLOCK".pdf

  #echo "$BLOCK < $(( FULLSTITCH * NSTITCH ))"

  # bad hack tmp.pdf temporary
  pdftk "$TOBIND" cat "$BLOCK"-"$ENDBLOCK" output tmp.pdf

  #echo ">>> created $TMP raw pages"

  # TODO replacce pdfbook2 -> latex backed by complicated pdftk
  pdfbook2 tmp.pdf

  echo ">>> stitched $TMP"

  mv tmp-book.pdf "$TMP"
  rm tmp.pdf

  STITCHED="$TMP $STITCHED"

  echo "$STITCHED"

  BLOCK=$(( BLOCK + NSTITCH ))
done

# merge booklets
OUT="${PDF/%.pdf/}"-stitch"$NSTITCH"booklet.pdf
pdftk $STITCHED cat output "$OUT"

echo
echo ">> DONE! - created $OUT"

# cleanup
rm "$TOBIND" blank.pdf $STITCHED
echo ">>> cleaned temporary files"
echo

exit 0
